FROM archlinux:base

WORKDIR /work

RUN pacman -Syu --noconfirm wget neofetch speedtest-cli iperf3 ethtool net-tools && \
  mkdir -p /work && \
  mkdir -p /results

COPY superbench.sh /work
