#!/bin/bash

run () {
  local file=/results/$(echo $1 | sed -e 's/[^A-Za-z0-9._-]/_/g').txt
  [ -e $file ] && rm $file
  touch $file
  echo
  wget -qO- $1 | bash | tee -a $file
}

iperf () {
  SERVERS=(
    "dal.speedtest.clouvider.net" "5200-5209" "10Gbps" "Dallas, Texas, USA" \
    "speedtest13.suddenlink.net" "5200-5209" "10Gbps" "Charleston, South Carolina, USA" \
    "speedtest.wobcom.de" "5200-5209" "2x25Gbps" "Wolfsburg, Germany" \
    "a204.speedtest.wobcom.de" "5200-5209" "2x25Gbps" "Amsterdam, Netherlands" \
    "speedtest.novoserve.com" "5202-5206" "40Gbps" "Amsterdam, Netherlands" \
    "iperf.par2.as49434.net" "9202" "40Gbps" "Paris, France"
  )

  for (( i = 0; i < 6; i++ )) do
    host="${SERVERS[i*4]}"
    ports="${SERVERS[i*4+1]}"
    speed="${SERVERS[i*4+2]}"
    location="${SERVERS[i*4+3]}"
    echo
    echo "host : ${host}"
    echo "ports : ${ports}"
    echo "speed : ${speed}"
    echo "location : ${location}"
    echo

    local retries=3
    
    for try in $( eval echo {1..$retries} ); do
      iperf3 -c ${host}
      [[ $? -eq 0 ]] && break
      if [ $try -lt $retries ]; then
        echo "iperf failed to connect to ${host}, retry ${try}"
      else
        echo "failed to run iperf against ${host}"
        echo
        break
      fi
    done
  done
}

interfaces () {
  for IFACE in $(ifconfig | cut -d ' ' -f1| tr ':' '\n' | awk NF); do
    if [ $IFACE != "lo" ]; then
      echo
      echo "interface : ${IFACE}"
      echo
      ethtool -i "$IFACE"
    fi
  done
}

main () {
  set -x
  neofetch
  set +x
  interfaces
  iperf
  set -x
  speedtest
  run yabs.sh
  run bench.sh
  run wget.racing/nench.sh
  run https://raw.githubusercontent.com/mgutz/vpsbench/master/vpsbench
}

time main | tee -a /results/superbench-$(date +"%Y_%m_%d_%I_%M_%p").txt
